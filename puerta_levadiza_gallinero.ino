#include <AccelStepper.h>
#include <DS3231.h>
#include <Wire.h>

DS3231 clock;

bool h12Flag;
bool pmFlag;
bool century = false;

AccelStepper stepper;
const int pinLdr = A0;
const int cantidadLuz = 60;

int estado = 0;
int ldr;

// Error de lectura para no confundir sombras o luces con noche o día
int contadorErrorLectura = 0;
int errorLectura = 100;

const int horaAperturaVerano = 8;
const int horaAperturaInvierno = 10;
const int horaCierreVerano = 21;
const int horaCierreInvierno = 18;

int horaApertura;
int horaCierre;

int estacion;
int verano = 1;
int invierno = 0;

void setup() {
  Serial.begin(9600);
  Wire.begin();

  stepper.setMaxSpeed(500.0);
  stepper.setAcceleration(900.0);
  pinMode(pinLdr, INPUT);
}

void loop() {

  // Si es verano voy a horario verano
  if (clock.getMonth(century) >= 6) {
    estacion = verano;
  }
  // sino a horario invierno
  else {
    estacion = invierno;
  }

  // si es verano uso su horaAperturaVerano
  if (estacion == verano) {
    horaApertura = horaAperturaVerano;
    horaCierre = horaCierreVerano;
  }
  // si es verano uso su horaAperturaInvierno
  if (estacion == invierno) {
    horaApertura = horaAperturaInvierno;
    horaCierre = horaCierreInvierno;
  }

  // Si es ya es de día en esa estación, abro
  if (clock.getHour(h12Flag, pmFlag) > horaApertura) {
    estado = 2;
  }

  // Si es de noche cierro
  if (clock.getHour(h12Flag, pmFlag) > horaCierre) {
    estado = 3;
  }
  if (clock.getHour(h12Flag, pmFlag) < horaCierre && 
     clock.getHour(h12Flag, pmFlag) > horaApertura) {
    estado = 1;
  }

  switch (estado) {

    case 1: // LEE
      ldr = analogRead(pinLdr);

      if (ldr > cantidadLuz) {
        contadorErrorLectura++;
        if (contadorErrorLectura > errorLectura) {
          estado = 2;
          contadorErrorLectura = 0;
        }
      }

      if (ldr < cantidadLuz) {
        contadorErrorLectura++;
        if (contadorErrorLectura > errorLectura) {
          estado = 3;
          contadorErrorLectura = 0;
        }
      }

      break;

    case 2: // ABRE
      stepper.runToNewPosition(0);
      stepper.runToNewPosition(2048);
      stepper.runToNewPosition(4096);
      //      stepper.disableOutputs();
      estado = 4;
      break;

    case 3: //CIERRA
      stepper.runToNewPosition(2048);
      stepper.runToNewPosition(0);
      stepper.disableOutputs();
      estado = 5;
      break;

    case 4: // ESTOY ABIERTO
      break;

    case 5: // ESTOY CERRADO
      break;
  }
}
